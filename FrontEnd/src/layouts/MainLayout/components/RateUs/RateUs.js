import React, { Component } from 'react'
import FlatButton from 'material-ui/FlatButton'
import Dialog from 'material-ui/Dialog'

import FacebookIcon from './assets/facebook.svg'
import GithubIcon from './assets/github.svg'
import TwitterIcon from './assets/twitter.svg'

import classes from './RateUs.scss'

class RateUs extends Component {
    constructor() {
        super()
    }

    render() {
        const { isOpen, toggle } = this.props

        const goToUrl = (url) => {
            window.open(url)
        }

        return (
            <div className={classes.stampIconContainer}>
                <img className={classes.stampIcon} src='./Datacom.jpg' width='96' height='48' title='Logo' onClick={() => goToUrl('https://datacomunity.sharepoint.com')}/>
            </div>
        )
    }
}

RateUs.propTypes = {
    isOpen: React.PropTypes.bool.isRequired,
    toggle: React.PropTypes.func.isRequired
}

export default RateUs

public void MapAttributesForExport(string FlowRuleName, MVEntry mventry, CSEntry csentry)
{
	string text = "";
	ArrayList arrayList = new ArrayList();
	string left = FlowRuleName.ToString();
	if (Operators.CompareString(left, "userAccountControlrule", false) == 0)
	{
		if (mventry.get_Item("employeeStatus").get_IsPresent())
		{
			if (csentry.get_Item("useraccountcontrol").get_IsPresent() & (Operators.CompareString(mventry.get_Item("employeeStatus").get_Value(), "ACTIVE", false) == 0))
			{
				csentry.get_Item("useraccountcontrol").set_IntegerValue(512L);
			}
			else
			{
				csentry.get_Item("useraccountcontrol").set_IntegerValue(514L);
			}
		}
	}
	else if (Operators.CompareString(left, "msExchHideFromAddressListsrule", false) == 0)
	{
		if (mventry.get_Item("employeeStatus").get_IsPresent())
		{
			if (csentry.get_Item("msExchHideFromAddressLists").get_IsPresent() & (Operators.CompareString(mventry.get_Item("employeeStatus").get_Value(), "ACTIVE", false) == 0))
			{
				csentry.get_Item("msExchHideFromAddressLists").set_BooleanValue(false);
			}
			else
			{
				csentry.get_Item("msExchHideFromAddressLists").set_BooleanValue(true);
			}
		}
	}
	else if (Operators.CompareString(left, "disableExchangePolicy", false) == 0)
	{
		if (mventry.get_Item("email").get_IsPresent() && csentry.get_Item("msExchPoliciesExcluded").get_IsPresent())
		{
			csentry.get_Item("msExchPoliciesExcluded").get_Values().Add("{26491cfc-9e50-4857-861b-0cb8df22b5d7}");
		}
	}
	else if (Operators.CompareString(left, "proxyAddressUpdate", false) == 0 && mventry.get_Item("email").get_IsPresent() && csentry.get_Item("proxyAddresses").get_IsPresent())
	{
		string text2 = "smtp:" + mventry.get_Item("email").get_Value();
		string[] array = csentry.get_Item("proxyAddresses").get_Values().ToStringArray();
		csentry.get_Item("proxyAddresses").Delete();
		string[] array2 = array;
		foreach (string text3 in array2)
		{
			string text4 = text3.ToString();
			int length = text4.Length;
			int num = text4.IndexOf("@");
			string left2 = text4.Substring(0, 4);
			if (Operators.CompareString(left2, "SMTP", false) == 0)
			{
				text4 = text4.Replace("SMTP", "smtp");
			}
			if ((Operators.CompareString(left2, "SMTP", false) == 0) | (Operators.CompareString(left2, "smtp", false) == 0))
			{
				arrayList.Add(text4.ToLower());
			}
			csentry.get_Item("proxyAddresses").get_Values().Add(text4);
		}
		if (arrayList.Contains(text2.ToLower()))
		{
			csentry.get_Item("proxyAddresses").get_Values().Remove(text2);
			string text5 = "SMTP:" + mventry.get_Item("email").get_Value();
			csentry.get_Item("proxyAddresses").get_Values().Add(text5);
		}
		else
		{
			string text5 = "SMTP:" + mventry.get_Item("email").get_Value();
			csentry.get_Item("proxyAddresses").get_Values().Add(text5);
		}
	}
}
